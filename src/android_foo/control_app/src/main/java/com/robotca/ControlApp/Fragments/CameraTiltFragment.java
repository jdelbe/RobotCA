package com.robotca.ControlApp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.robotca.ControlApp.ControlApp;
import com.robotca.ControlApp.Core.RobotController;
import com.robotca.ControlApp.R;

import org.ros.message.MessageListener;

import sensor_msgs.CompressedImage;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class CameraTiltFragment extends RosFragment {

    private SeekBar tiltBar;
    private View view;

    public CameraTiltFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the view if needed
        if (view == null) {
            view = inflater.inflate(R.layout.camera_tilt_bar_layout, container, false);

            tiltBar = (SeekBar) view.findViewById(R.id.camera_tilt_bar);
            tiltBar.setMax(180);
            tiltBar.setProgress(tiltBar.getMax() / 2);

            tiltBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // UI gives us [0:180]; but we want [-90:90]
                    ((ControlApp) getActivity()).getRobotController().publishCameraTiltAngleRequest(progress - 90);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }

        return view;
    }

    @Override
    void shutdown() {
    }
}

