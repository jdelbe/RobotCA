##---------------Begin: proguard configuration common for all Android apps ----------
##---------------End: proguard configuration common for all Android apps ----------

#---------------Begin: proguard configuration for support library  ----------
##---------------End: proguard configuration for support library  ----------

##---------------Begin: proguard configuration for Gson  ----------
##---------------End: proguard configuration for Gson  ----------

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
